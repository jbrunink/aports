# Contributor: Konstantin Kulikov <k.kulikov2@gmail.com>
# Maintainer: Konstantin Kulikov <k.kulikov2@gmail.com>
pkgname=grafana
pkgver=7.0.4
pkgrel=0
_commit=c4d8d0c2ae # git rev-parse --short HEAD
_stamp=1593067515 # git --no-pager show -s --format=%ct
pkgdesc="Open source, feature rich metrics dashboard and graph editor"
url="https://grafana.com"
arch="all !mips !mips64" # go is missing
license="Apache-2.0"
makedepends="go"
install="$pkgname.pre-install"
subpackages="$pkgname-openrc"
options="net chmod-clean"
source="$pkgname-$pkgver.tar.gz::https://github.com/grafana/grafana/archive/v$pkgver.tar.gz
	$pkgname-$pkgver-bin.tar.gz::https://dl.grafana.com/oss/release/grafana-$pkgver.linux-amd64.tar.gz
	$pkgname.initd $pkgname.confd"

export GOPATH=${GOPATH:-$srcdir/go}
export GOCACHE=${GOCACHE:-$srcdir/go-build}
export GOTMPDIR=${GOTMPDIR:-$srcdir}

# secfixes:
#   7.0.2-r0:
#     - CVE-2020-13379
#   6.3.4-r0:
#     - CVE-2019-15043

build() {
	local ldflags="-X main.version=$pkgver -X main.commit=$_commit -X main.buildstamp=$_stamp"
	go build -ldflags "$ldflags" -v github.com/grafana/grafana/pkg/cmd/grafana-server
	go build -ldflags "$ldflags" -v github.com/grafana/grafana/pkg/cmd/grafana-cli
}

check() {
	local pkgs="./..."
	case "$CARCH" in
	# Out of bounds test failure on 32bit archs, likely bug in test.
	# https://github.com/grafana/grafana/issues/25287
	armhf|armv7|x86) pkgs="$(go list ./... | grep -Ev '(pkg/tsdb$)|(pkg/services/alerting/conditions$)')" ;;
	esac

	go test $pkgs
}

package() {
	install -Dm755 "$srcdir/$pkgname.initd" "$pkgdir/etc/init.d/$pkgname"
	install -Dm644 "$srcdir/$pkgname.confd" "$pkgdir/etc/conf.d/$pkgname"
	install -Dm755 "$builddir/$pkgname-server" "$pkgdir/usr/sbin/$pkgname-server"
	install -Dm755 "$builddir/$pkgname-cli" "$pkgdir/usr/bin/$pkgname-cli"
	install -Dm644 "$builddir/conf/sample.ini" "$pkgdir/etc/grafana.ini"
	install -dm755 "$pkgdir/usr/share/grafana"
	cp -r "$builddir/conf" "$builddir/public" "$pkgdir/usr/share/$pkgname/"
}

sha512sums="cc73a3f368bf5ba959c5b6a8e8cfe7e8ffdf76d5a6ff068a99a20e62e42b0a14cbd61289c5b465e3f71edafd5027bd8c1eb2e504f698edc000bdb2d71dcb7a7a  grafana-7.0.4.tar.gz
b57704184307395d2c877bae7ad788bbbc50ae7b038a8a02915e5ad0e17b7ada3100e313a29939b908f23ef038f773c89c4877b4babe72dc9c7e3f66fc18198d  grafana-7.0.4-bin.tar.gz
b0a781e1b1e33741a97e231c761b1200239c6f1235ffbe82311fe883387eb23bef262ad68256ebd6cf87d74298041b53b947ea7a493cfa5aa814b2a1c5181e13  grafana.initd
c2d9896ae9a9425f759a47aeab42b7c43b63328e82670d50185de8c08cda7b8df264c8b105c5c3138b90dd46e86598b16826457eb3b2979a899b3a508cbe4e8c  grafana.confd"
